#define Icapa 2
#define Imonter 8
#define Idescendre 4

#define Pbuzz A5

#define fc A0
#define PboutonMH A2
#define PboutonMB A1
#define PboutonDH A4
#define PboutonDB A3

#define PredH 3
#define PgreenH 5
#define PblueH 6
#define PredB 11
#define PgreenB 10
#define PblueB 9

bool haut=false;
bool Bmanette=false;

int timer = 22000;

void setup() {
  pinMode(Icapa, OUTPUT);
  pinMode(Imonter, OUTPUT);
  pinMode(Idescendre, OUTPUT);
  
  pinMode(Pbuzz, OUTPUT);

  pinMode(PredH,OUTPUT);
  pinMode(PgreenH,OUTPUT);
  pinMode(PblueH,OUTPUT);
  pinMode(PredB,OUTPUT);
  pinMode(PgreenB,OUTPUT);
  pinMode(PblueB,OUTPUT);
  eteindre();  

  pinMode(PboutonMH,INPUT);
  pinMode(PboutonMB,INPUT);
  pinMode(PboutonDH,INPUT);
  pinMode(PboutonDB,INPUT);
}

void buzz(int t){
  digitalWrite(Pbuzz, HIGH);
  delay(t);
  digitalWrite(Pbuzz, LOW);
}

void stoper(){
  digitalWrite(Icapa, LOW);
  digitalWrite(Imonter, LOW);
  digitalWrite(Idescendre, LOW);
  buzz(100);
  delay(200);
  buzz(100);
}

void setorange(){
  eteindre();
  analogWrite(PredH,240);
  analogWrite(PgreenH,250);
  analogWrite(PredB,240);
  analogWrite(PgreenB,250);
}

void setred(){
  eteindre();
  analogWrite(PredH,0);
  analogWrite(PredB,0);
}

void monter(){
  setorange();
  buzz(1000);
  delay(1000);
    
  digitalWrite(Imonter, HIGH);
  digitalWrite(Icapa, HIGH);

  for(int i=0;i<timer;i++){
    timer--;
    delay(1);
    if (digitalRead(PboutonMB)==HIGH || digitalRead(PboutonMH)==HIGH || digitalRead(PboutonDB)==HIGH || digitalRead(PboutonDH)==HIGH){ 
      haut = false;
      break;
    }
  }
  timer=22000;
  eteindre();
  stoper();
}

void descendre(){
  timer=22000;
  eteindre();
  setorange();
  buzz(1000);
  delay(1000);

  digitalWrite(Idescendre, HIGH);
  digitalWrite(Icapa, HIGH);  

  while(digitalRead(fc)!=HIGH && digitalRead(PboutonMB)!=HIGH && digitalRead(PboutonMH)!=HIGH && digitalRead(PboutonDB)!=HIGH && digitalRead(PboutonDH)!=HIGH){
  }
  eteindre();
  stoper();
}

void eteindre(){
  digitalWrite(PredH,HIGH);
  digitalWrite(PgreenH,HIGH);
  digitalWrite(PblueH,HIGH);
  digitalWrite(PredB,HIGH);
  digitalWrite(PgreenB,HIGH);
  digitalWrite(PblueB,HIGH);
}

void loop() {
  analogWrite(PgreenH,250);
  analogWrite(PgreenB,250);
  
  if(digitalRead(PboutonMB)==HIGH || digitalRead(PboutonMH)==HIGH){
    delay(200);
    if(digitalRead(PboutonMB)==HIGH || digitalRead(PboutonMH)==HIGH){
      if (!haut){
        haut=true;
        monter();
      }
      else{
        setred();
        delay(1000);
        eteindre();
      }
    }
  }

  if(digitalRead(PboutonDB)==HIGH || digitalRead(PboutonDH)==HIGH){
    delay(200);
    if(digitalRead(PboutonDB)==HIGH || digitalRead(PboutonDH)==HIGH){
      if (!digitalRead(fc)){
        haut=false;
        descendre();
      }
      else{
        setred();
        delay(1000);
        eteindre();
      }
    }
  }
}
